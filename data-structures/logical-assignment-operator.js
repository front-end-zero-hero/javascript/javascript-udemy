const restaurant = {
    name: 'Classico Italiano',
    location: 'Via Angelo Tavanti 23, Firenze, Italy',
    categories: ['Italian', 'Pizzeria', 'Vegetarian', 'Organic'],
    starterMenu: ['Focaccia', 'Bruschetta', 'Garlic Bread', 'Caprese Salad'],
    mainMenu: ['Pizza', 'Pasta', 'Risotto'],

    openingHours: {
        thu: {
            open: 12,
            close: 22,
        },
        fri: {
            open: 11,
            close: 23,
        },
        sat: {
            open: 0, // Open 24 hours
            close: 24,
        },
    },

    orderDelivery: function ({starterIndex = 1, mainIndex = 0, time = "20:00", address}) {
        const message = `Order received! ${this.starterMenu[starterIndex]} and ${this.mainMenu[mainIndex]} will be delivered to ${address} at ${time}`;
        console.log(message);
    },
    orderPasta: function (ing1, ing2, ing3) {
        console.log(`Here is your delicious pasta with ${ing1}, ${ing2} and ${ing3}`);
    },
    orderPizza: function (mainIngredient, ...otherIngredients) {
        console.log(mainIngredient);
        console.log(otherIngredients);
    }
};

const rest1 = {
    name: 'Capri',
    numGuests: 0
}

const rest2 = {
    name: 'La Piazza',
    owner: 'Giovanni Rossi'
}

//
// rest1.numGuests ||= 10;
// rest2.numGuests ||= 10;

rest1.numGuests ??= 10;
rest2.numGuests ??= 10;

//rest1.owner = rest1.owner && "<ANONYMUS>" //  { name: 'Capri', numGuests: 0, owner: undefined }
rest1.owner &&= "<ANONYMUS>" // { name: 'Capri', numGuests: 0 }
rest2.owner &&= "<ANONYMUS>" // { name: 'La Piazza', owner: '<ANONYMUS>', numGuests: 10 }

console.log(rest1);
console.log(rest2);
