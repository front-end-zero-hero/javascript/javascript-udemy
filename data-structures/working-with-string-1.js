const airline = 'Tap Air Portugarl'
const plane = 'A320'
console.log(plane[0])
console.log(plane[1])
console.log(plane[2])
console.log('B737'[0])
console.log(airline.length)
console.log('B737'.length)

console.log(airline.indexOf('r'))

const message = 'Go to gate 23';
console.log(message.padStart(25, '+').padEnd(35, '+'))

const maskCreditCard = (number) => {
    const str = number + '';
    const last = str.slice(-4);
    return last.padStart(str.length, '*');
}

console.log(maskCreditCard(23154679813213321))