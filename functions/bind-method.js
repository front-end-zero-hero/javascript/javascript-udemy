const lufthansa = {
    airline: 'Lufthasa',
    iataCode: 'LH',
    booking: [],
    book(flightNum, name) {
        console.log(`${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`);
        this.booking.push({flight: `${this.iataCode}${flightNum}`, name});
    }
}

const eurowings = {
    airline: 'Eurowings',
    iataCode: 'EW',
    booking: []
}


const book = lufthansa.book;
const bookEW = book.bind(eurowings);
const bookLH = book.bind(lufthansa);

bookEW(23, 'Steven William');
const bookEW23 = book.bind(eurowings, 1234);
bookEW23('Johnas Schedmann');
bookEW23('Cooper');

lufthansa.planes = 300;
lufthansa.buyPlane = function() {
    console.log(this);
    this.planes++;
    console.log(this.planes);
}