const oneWord = function(str) {
    return str.replace(/ /g, '').toLowerCase();
}

const upperFirst = function(str) {
    const [first, ...others] = str.split(' ');
    return [first.toUpperCase(), ...others].join(' ');
}

const transformer = function (str, fn) {
    const result = fn(str);
    console.log(`Original string ${str}`);
    console.log(`Transformed string: ${result}`);
    console.log(`Transformed by: ${fn.name}`);
}

transformer('Javascript is the best', upperFirst);