const greet = function (greeting) {
    return function (name) {
        console.log(`${greeting} ${name}`);
    }
}

const greeterHey = greet('Hey');

console.log(greeterHey);

greeterHey('John');
greeterHey('Steve');
greet('Hey')('Tommy');

const greetArr = (greeting) => {
    return (name) => {
        console.log(`${greeting} ${name}`);
    }
}

const greetArr1 = greeting => name => {
    console.log(`${greeting} ${name}`);
}

greetArr1('Hey')('Jame');