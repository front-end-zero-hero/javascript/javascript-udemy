const lufthansa = {
    airline: 'Lufthasa',
    iataCode: 'LH',
    booking: [],
    book(flightNum, name) {
        console.log(`${name} booked a seat on ${this.airline} flight ${this.iataCode}${flightNum}`);
        this.booking.push({flight: `${this.iataCode}${flightNum}`, name});
    }
}

lufthansa.book(239, 'Jonas Schmedtmann');
lufthansa.book(635, 'Jonas Smith');
console.log(lufthansa);

const eurowings = {
    airline: 'Eurowings',
    iataCode: 'EW',
    booking: []
}

const book = lufthansa.book;
book.call(eurowings, 23, 'Sarah Williams');

book.call(lufthansa, 239, 'Mary Copper');
console.log(lufthansa);

const flightData = [583, 'George Copper'];
book.apply(eurowings, flightData);
book.call(eurowings, ...flightData);













